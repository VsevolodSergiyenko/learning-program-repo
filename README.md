# README #

This repository stores all the results of PTLP 2016-2017

### Contribution guidelines ###

* All homework tasks can be found on master branch
* Each user should create his base branch naming it using following pattern: Ivan Ivanov-> branch's name would be iivanov
* Each new module should be started on the separate branch, example: Ivan Ivanov studies NightwatchJS -> branch iivanov/nightwatchjs
* Module branch should be forked from base user's branch -> iivanov/nightwatchjs is forked from iivanov
* Each Module's branch should contain root folder with the name that matches to the tool name: branch iivanov/nightwatchjs has root folder nightwatchjs
* Once homework is ready, one must create a pull-request (PR) to his base branch adding everyone else as reviewers: Ivan Ivanov has finished hometask for NightwatchJS, so he pushed all stuff to iivanov/nightwatchjs and then created a PR iivanov/nightwatchjs to iivanov