'use strict';

var helpers = require('../../helpers.js');
var globals = require('../../globals.json');

var parentWindow;

module.exports = {

    // this runs once before the suite
    before: function (browser) {
        // kill localstorage for the browser
        browser.execute(function () {
            localstorage.clear();
        }, []);
    },

    "example test": function (browser) {
        browser
            //maximize browser window
            .windowMaximize()
            // go to the test page
            .url(globals.url)
            // wait for Canvas Prints element to be visible
            .waitForElementVisible(".hide-menu-top>ul>li:first-of-type", globals.timeout)
            //click Canvas Prints menu item
            .click(".hide-menu-top>ul>li:first-of-type")
            //explicit wait
            .pause(globals.timeout / 10)
            //close the browsa
            .end();
        console.log(helpers.doSmthHere());
    }
};
