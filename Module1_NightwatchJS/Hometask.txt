TODOs:
- Create a simple framework consisting of:
a) engine layer: using nightwatchjs as base browser automation tool (according to its native specs);
b) domain: page objects to represnt UI enteties;
c) functional tests layer
d) utils layer: generic stuff for tests
e) reporting layer: creating reports for test execution
f) framework should support both chrome and firefox (latest)

- Create a simple test with following steps:
1. Go to https://www.epam.com/
2. Click "Careers" top menu item
3. Scroll do to job search form
4. Check that "FIND YOUR DREAM JOB" is visible
5. Check that keywords field has placeholder "Keywords (optional):" displayed
6. Check that default city dropdown value is "Minsk"
7. Check that default role dropdown value is "All Teams & Roles"
8. Check that "Search" button is visible
9. Type "Test Automation" to keywords field
10. In filters dropdown check "Software Test Engineering" item (rest should be kept unchecked)
11. Click "Search" button
12. Wait for results to load
13. For each result table line check the following:
a) in 1st column each job title contains "Test" word combination
b) in 2nd column each item equals to "Software Test Engineering"
c) in 3rd column each item should be equal to "Minsk, Belarus"
d) in 4th column each item:
- is clickable
- contains "Apply" word
14*. Create custom nightwatchjs command to do steps 9-11 above
(possible prototype: browser.searchForTAJobAtEPAM("Test Automation", "Software Test Engineering");)
15**. Create custom nightwatchjs command(assert) for verification of content as in step 13
(possible prototype: browser.checkJobResultTable("Test Automation", "Software Test Engineering", "Minsk, Belarus", "Apply");)

Note: to make firefox working pls read the following:
http://stackoverflow.com/questions/32918999/set-firefox-preferences-in-nightwatch

Hometask is due to: 
Apr 21st, 2017, eod (final version off deliverables must be submitted to BitBucket)